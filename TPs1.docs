﻿Administration des Bases de Données
ZZ3 F2
TP 1:

A) Analyse d’une instance Oracle existante

1/. Connexion à la VM tp-zz3-oracle-29 avec le compte oracle/oracle

2/. On lance notre instance oracle comme suit :
sql> startup pfile = /u01/app/oracle/admin/orcl/pfile/init.ora.94201816856

3/. Notre instance oracle est la suivante :
sql> show instance
instance "local"

4/. Le nom et la taille de notre base de données peuvent être récupérés comme suit :
sql> show parameter
db-name : orcl
db-block-size : 8192

Ou bien à partir de v$parameter, une vue qui contient des informations sur le fichier de paramètres, lu au démarrage de la base :
sql> SELECT name, value FROM v$parameter WHERE name LIKE '%block%';
NAME						VALUE
---------------------------------------------------
db_block_buffers				0
db_block_checksum			TYPICAL
db_block_size				8192
db_file_multiblock_read_count	128
db_block_checking			FALSE

Le nom de la base de données peut être récupéré ainsi à partir de la vue v$database :
sql> SELECT name FROM v$database;
NAME
---------
ORCL

Le nom de notre instance oracle :
sql> SELECT instance_name FROM v$instance;
INSTANCE_NAME
----------------
orcl

5/. Les fichiers de données de la base de données :
sql> SELECT name, bytes, block_size FROM v$datafile;
NAME								BYTES			BLOCK_SIZE
----------------------------------------------------------------------
/u01/app/oracle/oradata/orcl/system01.dbf	713031680		8192
/u01/app/oracle/oradata/orcl/sysaux01.dbf	555745280		8192
/u01/app/oracle/oradata/orcl/undotbs01.dbf	110100480		8192
/u01/app/oracle/oradata/orcl/users01.dbf	5242880		8192
/u01/app/oracle/oradata/orcl/example01.dbf	104857600		8192

Les fichiers de contrôle :
sql> SELECT name, BLOCK_SIZE FROM v$controlfile;
NAME
------------------
/u01/app/oracle/oradata/orcl/control01.ctl
/u01/app/oracle/flash_recovery_area/orcl/control02.ctl

Les fichiers de redo log :
sql> SELECT * FROM v$logfile;

6/. 
sql> SELECT * FROM v$version;
Oracle Database 11g Enterprise Edition Release 11.2.0.1.0 - 64bit Production
PL/SQL Release 11.2.0.1.0 - Production
CORE	11.2.0.1.0	Production
TNS for Linux: Version 11.2.0.1.0 - Production
NLSRTL Version 11.2.0.1.0 - Production

Le version de notre instance :
sql> SELECT version FROM v$instance;
VERSION
-----------------
11.2.0.1.0

7/. Le nombre des vues de notre base de données :
sql> SELECT COUNT(*) FROM v$fixed_table where name LIKE 'V$%';
  COUNT(*)
----------
       525

B) Arrêt et démarrage d’une instance

1/. Arrêt de l’instance :
sql> SHUTDOWN

La valeur de db_block_size (la taille du bloc de la base de données) est non modifiable.

2/. Insérer une ligne dans la table EMP :
sql> INSERT INTO EMP(EMPNO, ENAME)
  	  VALUES (1,'youssef');

L’arrêt normal de la base :
sql> SHUTDOWN

Dans ce mode d’arrêt, la base de données ne sera pas arrêtée tant que tous les utilisateurs ne sont pas déconnectés. On attend que les utilisateurs se déconnectent.

sql> SHUTDOWN IMMEDIATE
Ce mode d'arrêt fait des rollbacks et déconnectent les utilisateurs donc les données seront perdus tant qu’on n’a pas fait commit.

3/. On remarque que les données insérées précédemment ne sont plus dans la base de données.

4/.
Arrêt de la base en mode transactionnel :
sql> SHUTDOWN TRANSACTIONAL
Ce mode attend la fin des transactions en cours et force par la suite un point de reprise et fermeture des fichiers.

5/. On n'arrive pas à se connecter

6/. Dès qu'on valide notre transaction, toutes les connexions seront fermées. 

C) Création d’une nouvelle base de données F5

1/. On crée les répertoires

2/.
$ touch initF5.ora

3/.

4/. On lance notre instance :
sql> STARTUP NOMOUNT pfile=/u01/app/oracle/admin/F5/pfile/initF5.ora

Puis on lance le script suivant pour la création de la base de données F5 :
sql> START createF5.sql

5/. Les fichiers ont bien été crées.

6/. 
$ SELECT username FROM dba_users;
La requête ne marche pas car la vue dba_users n'est pas encore crée.


D) Création de vues du dictionnaire de données et création des packages Standard

1/.
sql> START /u01/app/oracle/product/11.2.0.4/db_1/rdbms/admin/catalog.sql

2/. Les noms des utilisateurs :
$ SELECT username FROM dba_users;
USERNAME
------------------------------
SYS
SYSTEM
OUTLN

4/.
STARTUP UPGRADE pfile=/u01/app/oracle/admin/F5/pfile/initF5.ora.94201816856
START /u01/app/oracle/product/11.2.0.4/db_1/rdbms/admin/utlsampl.sql

5/.
SELECT username FROM dba_users;
USERNAME
------------------------------
OUTLN
SYSTEM
SYS
SCOTT



















TP 2

A) Les fichiers de contrôle

1/. Les fichiers de contrôle :
$ SELECT NAME FROM V$CONTROLFILE;
NAME
--------------------------------------------------------------------------------
/u01/app/oracle/oradata/F5/control01.ctl
/u01/app/oracle/flash_recovery_area/F5/control02.ctl

2/.
$ ALTER DATABASE BACKUP CONTROLFILE TO TRACE AS 'control.sql';

3/.
STARTUP UPGRADE pfile=/u01/app/oracle/admin/F5/pfile/initF5v2.ora.94201816856
ORACLE instance started.
Total System Global Area  217157632 bytes
Fixed Size		    2211928 bytes
Variable Size		  159387560 bytes
Database Buffers	   50331648 bytes
Redo Buffers		    5226496 bytes
ORA-00205: error in identifying control file, check alert log for more info

4/.
$ STARTUP UPGRADE pfile=/u01/app/oracle/admin/F5/pfile/initF5v2.ora.94201816856
ORACLE instance started.
Total System Global Area  217157632 bytes
Fixed Size		    2211928 bytes
Variable Size		  159387560 bytes
Database Buffers	   50331648 bytes
Redo Buffers		    5226496 bytes
Database mounted.
Database opened.

5/.
$ SELECT RECORD_SIZE, RECORDS_TOTAL, RECORDS_USED 
	FROM V$CONTROLFILE_RECORD_SECTION 
	WHERE TYPE LIKE 'DATAFILE';
RECORD_SIZE RECORDS_TOTAL RECORDS_USED
----------- ------------- ------------
	520	      100	     3

6/.

B) Les fichiers de reprise

1/. Le nombre de fichiers de reprise :
$ SELECT COUNT(*) AS nombre_fichier_reprise FROM V$LOGFILE;
NOMBRE_FICHIER_REPRISE
----------------------
		     3

Afficher les fichiers de contrôle par groupe :
$ SELECT GROUP#, MEMBER 
	FROM V$LOGFILE;
GROUP#				MEMBER
--------------------------------------------------------------------------------
1				/u01/app/oracle/oradata/F5/redo01.log
2				/u01/app/oracle/oradata/F5/redo02.log
3				/u01/app/oracle/oradata/F5/redo03.log

Le nombre de membre par groupe :
$ SELECT GROUP#, MEMBERS from v$log;
    GROUP#    MEMBERS
---------- ----------
	 1	    1
	 2	    1
	 3	    1

2/.
$ ARCHIVE LOG LIST
Database log mode	       No Archive Mode
Automatic archival	       Disabled
Archive destination	       USE_DB_RECOVERY_FILE_DEST
Oldest online log sequence     7
Current log sequence	       9

3/.
$ SELECT * FROM V$LOG WHERE STATUS='CURRENT';
GROUP#    THREAD#  SEQUENCE#      BYTES  B	LOCKSIZE	  MEMBERS   ARC    STATUS	FIRST_CHANGE#  FIRST_TIM NEXT_CHANGE# NEXT_TIME
---------- ---------- ---------- ---------- ---------- ---------- --- ---------------- ------------
3	    1	      9   	  52428800	   512		     1      NO     CURRENT	189866 		19-NOV-18   2.8147E+14

4/.

5/.
$ ALTER DATABASE ADD LOGFILE MEMBER '/home/oracle/Documents/dbm-labs/TP2/log1b.rdo' TO GROUP 1;
$ ALTER DATABASE ADD LOGFILE MEMBER '/home/oracle/Documents/dbm-labs/TP2/log2b.rdo' TO GROUP 2;
$ ALTER DATABASE ADD LOGFILE MEMBER '/home/oracle/Documents/dbm-labs/TP2/log3b.rdo' TO GROUP 3;

6/. Ajouter un groupe :
$ ALTER DATABASE ADD LOGFILE GROUP 4 ('D:\data\DISK2\log3c.rdo') SIZE 17;

7/.
$ 

8/. Supprimer un groupe :
$ ALTER DATABASE DROP LOGFILE GROUP 4;

9/.

C) Les fichiers de données et les tablespaces
1/. On crée les répertoires comme demandé

2/.
a)
$ CREATE TABLESPACE DATA01 
  DATAFILE '/u01/app/oracle/oradata/F5/DISK4/data01.dbf' SIZE 2M;

b)
$ CREATE TEMPORARY TABLESPACE TEMP 
  TEMPFILE '/u01/app/oracle/oradata/F5/DISK3/temp02.dbf' SIZE 5M
  AUTOEXTEND OFF;

c)
$ CREATE TABLESPACE INDX01 
  DATAFILE '/u01/app/oracle/oradata/F5/DISK2/indx01.dbf' SIZE 3M
  AUTOEXTEND ON NEXT 500K;

d)
$ CREATE TABLESPACE RONLY 
  DATAFILE '/u01/app/oracle/oradata/F5/DISK1/ronly01.dbf' SIZE 2M;
 
3/. On affiche le nom du tablespace, l’emplacement de son fichier de contrôle ainsi que sa taille :
$ SELECT TABLESPACE_NAME, FILENAME, BYTES 
  FROM DBA_DATA_FILES;
TABLESPACE_NAME		FILENAME					BYTES
--------------------------------------------------------------------------------
DATA01			/u01/app/oracle/oradata/F5/DISK4/data01.dbf
INDX01			/u01/app/oracle/oradata/F5/DISK2/indx01.dbf
RONLY			/u01/app/oracle/oradata/F5/DISK1/ronly01.dbf

$ SELECT TABLESPACE_NAME, FILENAME, BYTES 
  FROM DBA_TEMP_FILES;
TABLESPACE_NAME		FILENAME					BYTES
--------------------------------------------------------------------------------
TEMP			/u01/app/oracle/oradata/F5/DISK3/temp02.dbf

4/.
$ SELECT TABLESPACE_NAME, STATUS, CONTENTS
  FROM DBA_TABLESPACES
  WHERE CONTENTS = 'TEMPORARY';

5/. On définit un tablespace pa défaut :
$ ALTER DATABASE DEFAULT TABLESPACE DATA01;

On définit également un tablespace temporaire :
$ ALTER DATABASE DEFAULT TEMPORARY TABLESPACE TEMP;

6/. On modifie la taille de notre fichier de données :
$ ALTER DATABASE DATAFILE '/u01/app/oracle/oradata/F5/DISK4/data01.dbf' RESIZE 3M;

$ ALTER TABLESPACE DATA01 ADD DATAFILE '/u01/app/oracle/oradata/F5/DISK4/data02.dbf' SIZE 1M;

7/.
$ ALTER TABLESPACE INDX01 OFFLINE;
$ ALTER TABLESPACE INDX01 RENAME DATAFILE '/u01/app/oracle/oradata/F5/DISK2/indx01.dbf' TO '/u01/app/oracle/oradata/F5/DISK1/indx01.dbf';






















TP 3 :

A) Gestion des utilisateurs

1/.
$ CREATE USER bob IDENTIFIED BY along DEFAULT TABLESPACE DATA01 QUOTA 2 M ON DATA01 ACCOUNT UNLOCK;
 
2/.
$ GRANT CREATE SESSION, CREATE TABLE, UNLIMITED TABLESPACE TO kay IDENTIFIED BY mary;

3/.
$ CONNECT kay/mary;
$ CREATE TABLE EMP AS SELECT * FROM SCOTT.EMP;

4/. On affiche les utilisateurs de la base, on voit clairement que les deux utilisateurs BOB et KAY ont bien été crée.
$ SELECT USERNAME FROM DBA_USERS;
USERNAME
------------------------------
OUTLN
SYSTEM
SYS
KAY
SCOTT
BOB

$ SELECT USERNAME, BYTES FROM DBA_TS_QUOTAS WHERE DBA_USERS.USERNAME = 'BOB' OR DBA_USERS.USERNAME = 'KAY';
object DBA_TS_QUOTAS does not exist

5/.
$ SELECT USERNAME, DEFAULT_TABLESPACE FROM DBA_USERS WHERE USERNAME='KAY';
USERNAME		       DEFAULT_TABLESPACE
------------------------------ ------------------------------
KAY			       DATA01

6/. On modifie le quota de BOB et KAY :
$ ALTER USER kay QUOTA 5 M ON DATA01;
$ ALTER USER bob QUOTA 0 ON DATA01;

7/.
$ DROP USER kay CASCADE;

8/.
$ ALTER USER bob IDENTIFIED BY olink PASSWORD EXPIRE;
User altered.

B) Gestion des profils

1/.
$ SELECT * FROM DBA_PROFILES WHERE PROFILE='DEFAULT';

2/.
$ CREATE PROFILE profile LIMIT SESSIONS_PER_USER 2 IDLE_TIME 1;
Profile created.

$ SELECT * FROM DBA_PROFILES WHERE PROFILE='PROFILE';

3/.
$ ALTER USER bob PROFILE PROFILE;
User altered.

4/.
$ CONNECT bob/along;

G)
1/.
$ CONNECT SCOTT/tiger
Connected.

$ GRANT SELECT ON EMP TO KAY;
Grant succeeded.

$ GRANT SELECT ON SCOTT.DEPT TO KAY;
Grant succeeded.

2/.
$ CONNECT KAY/mary;
Connected.

$ CREATE TABLE EMP AS SELECT * FROM SCOTT.EMP;
Table created.

$ CREATE TABLE DEPT AS SELECT * FROM SCOTT.DEPT;
Table created.

3/.
$ GRANT SELECT ON EMP TO BOB WITH GRANT OPTION;
Grant succeeded.

$ SELECT GRANTEE, OWNER, TABLE_NAME, GRANTOR, PRIVILEGE, GRANTABLE FROM DBA_TAB_PRIVS;

4/.
$ CREATE USER todd IDENTIFIED BY todd;

$ CONNECT BOB/olink;

$ GRANT SELECT ON KAY.EMP TO TODD;

5/.
$ CONNECT KAY/mary;

$ REVOKE SELECT ON EMP FROM BOB;
Revoke succeeded.

$ REVOKE SELECT ON DEPT FROM BOB;
cannot REVOKE privileges you did not grant

$ CONNECT TODD/todd;

$ SELECT * FROM KAY.EMP;
table or view does not exist
L'utilisateur TODD n'a plus accès à la table EMP de KAY.

6/.
$ GRANT CREATE ANY TABLE TO KAY;
Grant succeeded.

$ CONNECT KAY/mary;

$ CREATE TABLE BOB.EMP AS SELECT * FROM EMP;

7/.
$ GRANT SYSOPER TO KAY;
Grant succeeded.

$ CONNECT KAY/mary AS SYSOPER;

D) Gestion des rôles

1/.
$ SELECT * FROM DBA_SYS_PRIVS WHERE GRANTEE='RESOURCE';

GRANTEE 		       PRIVILEGE				ADM
------------------------------ ---------------------------------------- ---
RESOURCE		       CREATE SEQUENCE				NO
RESOURCE		       CREATE TRIGGER				NO
RESOURCE		       CREATE CLUSTER				NO
RESOURCE		       CREATE PROCEDURE 			NO
RESOURCE		       CREATE TYPE				NO
RESOURCE		       CREATE OPERATOR				NO
RESOURCE		       CREATE TABLE				NO
RESOURCE		       CREATE INDEXTYPE 			NO

8 rows selected.

2/.
$ CREATE ROLE DEV;
Role created.

$ GRANT CREATE TABLE, CREATE VIEW TO DEV;
Grant succeeded.

$ GRANT SELECT ON EMP TO DEV;
Grant succeeded.

$ SELECT * FROM DBA_ROLES;
ROLE			       PASSWORD AUTHENTICAT
------------------------------ -------- -----------
CONNECT 		       NO	NONE
RESOURCE		       NO	NONE
DBA			       NO	NONE
SELECT_CATALOG_ROLE	       NO	NONE
EXECUTE_CATALOG_ROLE	       NO	NONE
DELETE_CATALOG_ROLE	       NO	NONE
EXP_FULL_DATABASE	       NO	NONE
IMP_FULL_DATABASE	       NO	NONE
DEV			       NO	NONE

$ SELECT * FROM DBA_SYS_PRIVS WHERE GRANTEE='DEV';
GRANTEE 		       PRIVILEGE				ADM
------------------------------ ---------------------------------------- ---
DEV			       CREATE TABLE				NO
DEV			       CREATE VIEW				NO

3/.
$ GRANT RESOURCE, DEV TO BOB;
Grant succeeded.

$ SELECT * FROM DBA_ROLE_PRIVS WHERE GRANTEE = 'BOB';
GRANTEE 		       GRANTED_ROLE		      ADM DEF
------------------------------ ------------------------------ --- ---
BOB			       RESOURCE 		      NO  YES
BOB			       DEV			      NO  YES

4/.
$ ALTER USER BOB DEFAULT ROLE DEV;
User altered.

TP 4

1/.
On identifie les fichiers de la base de données :
$ SELECT name FROM V$DATAFILE;
NAME
--------------------------------------------------------------------------------
/u01/app/oracle/oradata/F5/system01.dbf
/u01/app/oracle/oradata/F5/sysaux01.dbf
/u01/app/oracle/oradata/F5/undotbs01.dbf
/u01/app/oracle/oradata/F5/DISK4/data01.dbf
/u01/app/oracle/oradata/F5/DISK2/indx01.dbf
/u01/app/oracle/oradata/F5/DISK1/ronly01.dbf
6 rows selected.

$ SELECT member FROM V$LOGFILE;
MEMBER
--------------------------------------------------------------------------------
/u01/app/oracle/oradata/F5/redo01.log
/u01/app/oracle/oradata/F5/redo02.log
/u01/app/oracle/oradata/F5/redo03.log
6 rows selected.

$ SELECT name FROM V$CONTROLFILE;
NAME
--------------------------------------------------------------------------------
/u01/app/oracle/oradata/F5/control01.ctl
/u01/app/oracle/flash_recovery_area/F5/control02.ctl

On arrêter après la base de données :
$ SHUTDOWN NORMAL

Puis on sauvegarde tous ces fichiers : 
$ cp -p /u01/app/oracle/oradata/F5/* /home/oracle/Documents/dbm-labs/TP4/SAVE/
$ cp -p /u01/app/oracle/oradata/F5/DISK*/* /home/oracle/Documents/dbm-labs/TP4/SAVE/

Puis on redémarre notre base.

2/.
$ ALTER DATABASE ARCHIVELOG;
Fichiers de reprise archivés, permet de garder
toutes les modifications de la base.

3/.
Sauvegarde à chaud :

Identification des fichiers de données d'un tablespace (BD ouverte) :
$ SELECT FILE_NAME FROM DBA_DATA_FILES WHERE TABLESPACE_NAME = 'DATA01';

Mise OFFLINE du tablespace
$ ALTER TABLESPACE DATA01 OFFLINE NORMAL;

Sauvegarder les fichiers concernés

Mise ONLINE du tablespace :
$ ALTER TABLESPACE DATA01 ONLINE;

4/.
Identification des fichiers de données d'un tablespace (BD ouverte) :
$ SELECT FILE_NAME FROM DBA_DATA_FILES WHERE TABLESPACE_NAME = 'DATA01';

Début de la sauvegarde :
$ ALTER TABLESPACE DATA01 BEGIN BACKUP;

Sauvegarder les fichiers concernés

Fin de la sauvegarde :
$ ALTER TABLESPACE DATA01 END BACKUP;

5/.
a)
$ CONNECT scott/tiger
$ INSERT INTO EMP (EMPNO,ENAME) VALUES (1,'YOUSSEF');
$ COMMIT
$ INSERT INTO EMP (EMPNO,ENAME) VALUES (1,'HAMZA');

b)
$ SHUTDOWN ABORT

c)

d)
- Les blocs de données modifiées par une transaction validée peuvent
ne pas être écrits sur les fichiers de données (ils apparaissent
obligatoirement dans les fichiers de reprise).
- Les blocs de données modifiées par une transaction non validée
peuvent au contraire être écrits sur les fichiers de données (et dans les
fichiers de reprise).

e)
RECOVER FROM ...,...,..
DATABASE USING BACKUP CONTROLFILE









 

